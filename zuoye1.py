# 1、写一个注册的程序
# 1、注册的账号密码存在文件里面
# 2、密码不能为纯数字
# 3、已经存在的账号要提示该账号已经被注册
# 4、账号和密码长度要大于6小于13位
# 5、两次密码输入一致才可以
# 6、要校验输入不能为空

#思路分析：
#1.读取文件，用户名和密码在文件中都生成一个列表
#2.输入账户和密码和确认密码，判断是否为空
#3.若为空，给出提示
#4.若不为空，判断账户是否在列表中
#5.若在列表中，给出提示账户已存在；
#6.若不在列表中，判断账户长度
#7.若账户长度不符合要求，给出提示
#8.若账户长度符合要求，判断密码是否为纯数字
#9.若密码是纯数字，给出提示
#10.若密码你不是纯数字，判断密码长度
#11.用密码长度不符合要求，给出提示
#12.若密码长度符合要求，判断和确认密码是否一致
#13.若密码和确认密码不一致，给出提示
#14.若密码和确认密码一致，注册成功，结束

# f=open('names.txt','a+',encoding='utf-8')
# res = f.read()
# print(res)
#f.close()

f = open('zhuce.txt', 'r', encoding='utf-8')
res = f.read()
print('从文件读取出来的数据:',res)
f.close()
u = res.replace('\n',',')
user_list = u.split(',')
print(user_list)

while True:
    username = input('请输入用户名：').strip()
    password = input('请输入密码：').strip()
    cpassword = input('请确认密码：').strip()

    if username == '' or password == '':
        print('账户/密码不能为空！')
        continue
    else:
        # if username in user_list: #这样写不合理 如果密码和账户要是一致的话 就没法判断
        for index in range(0,len(user_list),2):
            user = user_list[index]
        if username == user:
                print('账户已存在！')
                continue
        else:
            if len(username) < 6 or len(username) > 13:
                print('账户的长度不能大于13位或者小于6位！')
                continue
            else:
                if password.isdigit() is True:
                    print('密码不能为纯数字！')
                    continue
                else:
                    if len(password) < 6 or len(password) > 13:
                        print('密码的长度不能大于13位或者小于6位！')
                        continue
                    else:
                        if password != cpassword:
                            print('两次密码输入不一致！')
                            continue
                        else:
                            print('注册成功！')
                            f = open('zhuce.txt', 'a+', encoding='utf-8');
                            f.write(username + ',');
                            f.write(password);
                            f.write('\n');
                            f.close();
    break

