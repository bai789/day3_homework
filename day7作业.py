# http: // q4.qlogo.cn / g?b = qq & nk = 1049328922 & s = 140  # 这个是获取头像的url
# 1、请求qq群的接口，实现传入一个群号
# 2、把群里每个人的
# 昵称、群备注、入群时间、qq号，性别，存到redis里面, 用hash类型
# {"qq_num": 511402865, "nick": "朝花夕拾", "card": "牛牛", "gender": "男", "入群时间": "2017-01-03"}
# 3、把每个人的头像下载下来保存到本地，牛牛.jpg
# 4、把昵称、群备注、入群时间、qq号，性别这些信息写到excel里面
# 5、把excel当做附件发到我的邮箱里面。

import requests
import redis
import time
import json
import xlrd
import xlwt
import yagmail

#调接口取成员信息
url = 'https://qun.qq.com/cgi-bin/qun_mgr/search_group_members'
data = {'gc':'921144696','st':'0','end':'20','sort':'0','bkn':'2078398053'}
cookie = {'cookie':'pgv_pvi=9247227904; pgv_pvid=7146279160; RK=vNxIiK7Baq; ptcz=b36db065a028a291de3c5e16de1e0715b55b2ddef8a5b6bc22869c8cd311d68e; o_cookie=274523046; pac_uid=1_274523046; pgv_si=s260190208; _qpsvr_localtk=0.6536187606827104; uin=o0274523046; p_uin=o0274523046; traceid=5886ee165a; skey=@S9Yydo2d9; ptisp=ctc; pt4_token=TEqQgGOYJIMDQ3FHqAcZx69AttLL6WN0QFO726jbGj0_; p_skey=0guO8svQN4rbhyJQQCTNMTsYvC60qC6FHTvmAssZZUk_'}
req = requests.post(url,data,cookies = cookie)
msg = req.json()
# print(msg)
# print(msg['mems'])


# 链接redis
host = '118.24.3.40'
passwd = 'HK139bc&*'

r = redis.Redis(host=host,password=passwd,db=6,decode_responses=True)

 # 取成员信息存入redis
for i in msg['mems']:
    # print(i)
    # print(i['nick'])
    time_tuple = time.localtime(i['join_time'])
    res = time.strftime('%Y-%m-%d',time_tuple)
    if i['g'] ==0:
        i['g']='男'
    elif i['g'] ==1:
        i['g'] = '女'
    else:
        i['g'] = '未知'
    # print((i['nick'],i['card'],res,i['uin'],i['g']))
    p = r.pipeline()
    d = {"nick": i['nick'], "card": i['card'], "入群时间": res, "qq_num": i['uin'], "gender": i['g']}
    k = i['card']
    value = json.dumps(d)
    p.hset('mem_info',k,value)
    p.execute()


#把成员信息写到excel里面
book = xlwt.Workbook()
sheet = book.add_sheet('sheet')

row = 0
for i in msg['mems']:
    # print(i)
    # print(i['nick'])
    time_tuple = time.localtime(i['join_time'])
    res = time.strftime('%Y-%m-%d',time_tuple)
    if i['g'] ==0:
        i['g']='男'
    elif i['g'] ==1:
        i['g'] = '女'
    else:
        i['g'] = '未知'
    # print((i['nick'],i['card'],res,i['uin'],i['g']))
    info =(i['nick'],i['card'],res,i['uin'],i['g'])
    col = 0
    for t in info:
        sheet.write(row, col, t)
        col+=1
    row+=1

book.save('info.xls')


#把excel发邮件
user = 'uitestp4p@163.com'
passwd = 'houyafan123' #授权码

smtp_host ='smtp.163.com'
mail = yagmail.SMTP(user=user,password=passwd,host=smtp_host) #连上邮箱
mail.send(to=['274523046@qq.com','511402865@qq.com'],
          cc=['274523046@qq.com','1392703098@qq.com'],
          subject='day7work',
          contents='辛苦辛苦查看下作业',
          attachments='info.xls' #附件只有一个的时候 不用列表
          )


# 下载头像
for l in msg['mems']:
    tx = l['uin']
    # print(tx)
    url= 'https://q4.qlogo.cn/g?b=qq&nk=%s&s=140' %tx   #就因为我给%s加了‘’导致url不对 取的头像全是企鹅。。。
    print(url)
    req = requests.get(url)
    with open('%s.jpg' %l['card'],'wb') as fw:
        fw.write(req.content)


