# 3、产生一批测试账号，存到文件里面
# 1、输入一个数字，就产生多少条
# 2、长度是10，账号包含字母和数字
# sdfs234sdf @ 163.com
# sdfs234sdf @ 163.com
# sdfs234sdf @ 163.com

#思路分析：
#1.通过循环产生多少条
#2.长度包含数字和字母，用join连接
#3.判断长度不能是10
#4.产生的数据写入文件

import string
import random

number_s = ['0','1','2','3','4','5','6','7','8','9']
letter_s=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

number =int(input('请输入一个整数：'))

i=0
while i<number:
    number_s = ''.join(random.sample(number_s, 5)).strip(',''')
    letter_s = ''.join(random.sample(letter_s, 5)).strip(',''')
    shuju = number_s +letter_s + '@ 163.com'
    i+=1
    f = open('shuju.txt','a',encoding='utf-8')
    f.write(shuju+'\n')
    f.close()

