# 1、写一个管理商品的程序
# 1、商品存在文件里面
# 2、添加商品的时候，商品存在的就不能添加了，数量只能是大于0的整数，价格可以是小数、整数，但是只能是大于0的
# 商品名称
# 商品价格
# 商品数量
# 3、删除的商品的时候输入商品名称，商品不存在，要提示
# 4、修改商品的时候
# 商品名称
# 商品价格
# 商品数量
# 5、查看商品，输入商品名称，查询到商品的信息

#思路分析：

#1.商品以字典的形式存在文件里面 商品={“商品名称”：xxx，“商品价格”：xxx，“商品数量”：xxx}
  #要用json.dump 写入文件
#2.添加商品
   # 1.判断商品是否存在 用商品名称是否在文件里，用for循环，存在给出提示，不存在，写入文件
   # 2.判断写入的 商品数量 必须是大于0 的整数，小于0 给出提示
   # 3.判断写入的 商品价格 可以是小数或者整数，但是要大于0  小于0 给出提示
#3.删除商品
   # 1.判断商品是否存在 用商品名称是否在文件里，用for循环，存在删除这条记录，不存在，给出提示
#4.修改商品
   # 1.修改商品的时候，修改完新的数据写入文件
#5.查看商品
   # 通过输入商品名称 读出来该行记录

import json
file='product.json'

#判断是否是整数和小数以及大于0：[商品价格]
def is_int_float(str):
    if str.count('.')==1:
        left,right = str.split('.')
        if left.isdigit() and right.isdigit():
            return True
        else:
            return False
    elif str.isdigit() and int(str) >0:
        return True
    return False


#判断是否是一个大于0的正整数：[商品数量]
def is_int(znum):
    znum=str(znum)
    if znum.isdigit() and int(znum) > 0:
        return True
    else:
        return False

#判断商品是否存在
def is_sp(spname):
    resut=check_sp(file)
    if check_sp(file) and spname in resut.keys():
        return True
    else:
        return False

#查看商品(读文件)
def check_sp(file):
    with open(file,'a+',encoding='utf-8') as f:
        f.seek(0)
        res = f.read()
        if len(res)!=0:
            d = json.loads(res)
            return True
        else:
            return False
#写入文件
def write_sp(d,file):
    with open(file,'w',encoding='utf-8') as fw:
        json.dump(d,fw,indent=4,ensure_ascii=False)
        fw.flush()

#添加商品
def add_sp(spname,spjg,spsl):
    if is_sp(spname):
        print('商品已存在')
    elif check_sp(file):
        d=check_sp(file)
        d[spname] = {'商品价格': spjg, '商品数量': spsl}
        write_sp(d,file)
        print("添加成功")
    elif not is_sp(spname):
        d1={}
        d1[spname]={'商品价格':spjg,'商品数量':spsl}
        write_sp(d1,file)
        print('添加成功')


#删除商品
def del_sp(spname):
    if is_sp(spname):
        res = check_sp(file)
        res.pop(spname)
        write_sp(res,file)
        print('删除成功')
    else:
        return False

#修改商品
def xg_sp(spname):
    if is_sp(spname):
        new_d = check_sp(file)
        x_spjg = input('修改价格为：').strip()
        x_spsl = input('修改数量为：').strip()
        new_d[spname]= {'商品价格': x_spjg, '商品数量': x_spsl}
        write_sp(new_d,file)
        return True
    else:
        return False

chioce = input('请输入你的选择：1.添加商品2.删除商品3.修改商品4.查看商品5.退出：').strip()

if chioce.isdigit():
    if int(chioce)==1:
        sp_n = input('商品名称：').strip()
        sp_j = input('商品价格：').strip()
        sp_s = input('商品数量：').strip()
        if is_int(sp_s) and is_int_float(sp_j):
            add_sp(sp_n,sp_j,sp_s)
        else:
            print('商品数量/商品价格/商品名称 不能为空')
    elif int(chioce)==2:
        sp_s1=input('请输入要删除的商品名称：').strip()
        del_sp(sp_s1)
    elif int(chioce)==3:
        spname=input('请输入要修改的商品名称：').strip()
        if spname.strip():
            xg_sp(spname)
        else:
            print('修改不成功')
    elif int(chioce)==4:
        spname = input('请输入商品名称：').strip()
        if is_sp(spname):
            spxx = check_sp(d)
            print('%s的相关信息%s' %(str(spname),str(spxx)))
        else:
            print('商品不存在')
    elif int(chioce)==5:
            quit('程序退出')
    else:
        print('请输入指定选项')
else:
    print('请输入指定选项')
