# 1、写一个程序实现登录，最多登录3次
# 2、如果登录成功，提示，欢迎xx登录，程序结束
# 3、如果登录失败，提示账号/密码错误，继续登录
# 4、要判断输入是否为空，如果输入为空要提示账号/密码不能为空，算错误一次

#思路分析：
#1.先去文件中读取账户和密码
#2.判断输入的账户密码是否在文件中（转换成字典）
#3.若账户或者密码输入为空，给出提示
#4.若账户密码输入错误，（不在文件中）提示账户或者密码输入错误
#5.输入超过三次，跳出登录，给出提示 （for循环）
#6.登录成功，给出提示，欢迎xx登录

f=open('zhuce.txt','r',encoding='utf-8')
res = f.read()
u = res.replace('\n',',')
user_list = u.split(',')
f.close()
print(user_list)

#将列表转化成字典
users_dict = {}
for i in range(0,len(user_list),2):
    users_dict[user_list[i]] = user_list[i+1]
print(users_dict)

count =0
while count < 3:
    count+=1

    username=input('请输入登录帐号：').strip()
    password=input('请输入登录密码：').strip()

    if username=='' or password=='':
        print('账户/密码不能为空！')
    elif username not in users_dict.keys() or password not in users_dict.values():
        print('账户/密码 输入错误！')
    else:
        print('欢迎%s登录！'%username)
        break
else:
    print('登录次数已经用尽！')



