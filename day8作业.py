# 作业：
#     1、登录接口，生成一个seesion_id，存到redis里面  login
#         redis的key是
#             session:md5(username)  => '已登录',失效时间自己设
#       入参：username,password
#       登录成功返回：{"seesion":"sdfsdfsdfsdf","code":0,"msg":"登录成功"}
#     2、添加qq群信息的接口， #add_mem
#         qq_mem表 ：id，qq，nick、join_time,gender,card
#         qq号是唯一的
#         入参：
#             qq，nick、join_time,gender,card，session
#         首先验证seesion有没有失效/对不对，如果session是正确的，再做操作
#         否则提示未登录

import flask,pymysql,json,hashlib,redis

#操作数据库函数
def op_mysql(sql:str):
    mysql_info = {
        'host': '118.24.3.40',
        'port': 3306,
        'password': '123456',
        'user': 'jxz',
        'db': 'jxz',
        'charset': 'utf8',
        'autocommit': True
    }
    result = '执行完成'
    conn = pymysql.connect(**mysql_info)
    cur = conn.cursor(pymysql.cursors.DictCursor) #建立游标
    cur.execute(sql)
        # result  = cur.fetchone()
    result  = cur.fetchall()
    cur.close()
    conn.close()
    return result

#用来给username加密，存储session
def md5(s,):
    s = (str(s)+'#$%$%').encode()
    m = hashlib.md5(s)#加密
    return m.hexdigest()

server = flask.Flask(__name__)

#登录 函数 #用户表是之前作业建的 就不在建新表了
@server.route('/login',methods=['post'])
def login():
    username = flask.request.json.get('username')
    password = flask.request.json.get('password')
    if username and password:
        sql = 'select * from bai_myuser where username = "%s"' % username
        if op_mysql(sql):
            pwd_sql = 'select "%s" from bai_myuser where username = "%s"' %(password,username)
            if op_mysql(pwd_sql):
                session = md5(username)
                write_redis(session)
                data = {'session':session,'code':'200','msg': '登录成功！'}
            else:
                data = {'msg': '密码输入错误！'}
        else:
            data = {'msg':'用户不存在！'}
    else:
        data = {'msg': '必填参数未填，请查看接口文档！'}
    return json.dumps(data, ensure_ascii=False)

# session存入redis
def write_redis(se):
    host = '118.24.3.40'
    passwd = 'HK139bc&*'
    r = redis.Redis(host=host,password=passwd,db=5,decode_responses=True)#db用来指定哪个数据库，默认用第一个数据库 #0-15
    return r.set('session_by:%s'%se, '已登录', 500)

#建qq_mem表
# qq_mem表 ：id，qq，nick、join_time,gender,card
# CREATE TABLE `bai_qq_mem` (
#   `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
#   `qq` varchar(30) NOT NULL UNIQUE,
#   `nick` varchar(32),
#   `join_time` DATETIME NOT NULL,
#   `gender` varchar(32) NOT NULL,
#   `card` varchar(32) NOT NULL
# )DEFAULT CHARSET=utf8;

#从redis取session
def get_session(se):
    host = '118.24.3.40'
    passwd = 'HK139bc&*'
    r = redis.Redis(host=host,password=passwd,db=5,decode_responses=True)#db用来指定哪个数据库，默认用第一个数据库 #0-15
    return r.get('session_by:%s'%se)


#添加qq群信息的接口， #add_mem 入参qq，nick、join_time,gender,card，session

@server.route('/add_mem',methods=['post'])
def add_mem():
    qq = flask.request.values.get('qq')
    nick = flask.request.values.get('nick')
    join_time = flask.request.values.get('join_time')
    gender = flask.request.values.get('gender')
    card = flask.request.values.get('card')
    session = flask.request.values.get('session')
    if get_session(session):
        sql = 'select * from bai_qq_mem where qq="%s"'%qq
        if op_mysql(sql):
            data = {'msg': 'qq号已经存在！'}
        else:
            insert_sql = 'insert into bai_qq_mem(qq,nick,join_time,gender,card) value ("%s","%s","%s","%s","%s")' % (qq, nick,join_time,gender,card)
            op_mysql(insert_sql)
            data = {'code':'200','msg': '添加成功！'}
    else:
        data = {'msg': '用户未登录！'}
    return json.dumps(data, ensure_ascii=False)

server.run(debug=True)