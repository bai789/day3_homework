   # 作业1：
   #      1、注册、登录
   #           表，自己建，error_count
   #          1、注册的时候存账户密码到数据库里面，密码存密文，要加盐
   #          2、登录的时候账号、密码从数据里面取，登录失败一次，错误次数+1，错误次数大于3不让登录


import pymysql
import hashlib

#调用产生的用户和密码插入数据库
def op_mysql(sql):
       conn = pymysql.connect(
           host='118.24.3.40',
           port=3306,
           user='jxz',
           password='123456',
           db='jxz',
           charset='utf8',
           autocommit = True
       )
       cur = conn.cursor()
       cur.execute(sql)
       result = cur.fetchall()
       cur.close()
       conn.close()
       return result

#给密码和确认密码加密文和加盐
def md5(jmpwd):
    salt = '#$%&'
    jmpwd = (str(jmpwd)+salt).encode()
    m = hashlib.md5(jmpwd)
    return m.hexdigest()

# 建立表
create_sql= "CREATE TABLE `bai_myuser` (`id` int(11) NOT NULL AUTO_INCREMENT,`username` varchar(30) NOT NULL,`passwd` varchar(32) NOT NULL,`cpasswd` varchar(32) NOT NULL,error_count int(10) default '0',PRIMARY KEY (`id`),UNIQUE KEY `username` (`username`)) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=utf8;"
op_mysql(create_sql)

#注册
for i in range(3):
    u = input('username:').strip()
    pwd = input('password:').strip()
    pwd2 = input('cpwd:').strip()
    if len(u)<6 or len(u)>13:
        print('用户名长度不合法')
    elif len(pwd)<6 or len(pwd)>13:
        print('密码长度不合法')
    elif pwd.isdigit():
        print('密码不能是纯数字')
    elif pwd!=pwd2:
        print('两次输入的密码不一致')
    else:
        select_sql = "select username from bai_myuser where username ='%s'"%u
        res = op_mysql(select_sql)
        if res:
            print("当前用户已存在，请重新输入！")
        else:
            jmpwd = md5(pwd)
            jmpwd2 = md5(pwd2)
            inser_sql ="insert into bai_myuser(username,passwd,cpasswd) values ('%s','%s','%s')"%(u,jmpwd,jmpwd2)
            op_mysql(inser_sql)
    break

#修改数据错误更新
def error_count(username,i=0):
    i = int(op_mysql("select error_count from bai_myuser where username='%s'" %username)[0][0])+1
    update_sql = "update bai_myuser set error_count='%s' where username ='%s'" % (i, username)
    op_mysql(update_sql)


#登录
count = 0
while count<3:
    count+=1
    username = input('username:').strip()
    if username =='':
        print('输入用户名不能为空！')
    else:
        select_u = "select username from bai_myuser where username ='%s'" % username
        res = op_mysql(select_u)
        if res is False:
            print("输入的用户名不存在！")
        else:
            password = input('password:').strip()
            if password=='':
                print('输入的密码不能为空！')
            else:
                select_pwd = "select passwd from bai_myuser where username ='%s'" % username
                res1 = op_mysql(select_pwd)[0]
                if password not in res1:
                    print("输入的密码不正确！")
                    error_count(username,0)
                else:
                     print("登录成功")
                     break
else:
    print("输入错误的次数已用尽！")





