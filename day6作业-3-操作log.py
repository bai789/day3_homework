#      3、删除，logs目录下3天前的日志，保留今天、昨天、前天。

import os
import time

time_tuple = time.localtime(time.time()-(3*24*60*60)) #把当前时间转换成时间元组
h_time = time.strftime('%Y-%m-%d',time_tuple) #把时间元组转换成格式化好的时间
# print(h_time)

for cur_dir,dirs,files in os.walk(r'E:\LionUIAuto\day6\logs'):
    for f in files:
        if f.endswith('.log'):
            left,right = f.split('_')
            left,right = right.split('.') #取到文件名称中的日期
            # print(left)
            if left < h_time: #日期和当前时间作比较
                real_path = os.path.join(cur_dir,f)
                # print(real_path)
                # print(f)
                os.remove(real_path) #删除文件


