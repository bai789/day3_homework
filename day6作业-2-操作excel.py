#      2、写一个函数，传入一个表名，就把这个表里面所有的数据全部导出到一个excel里面来

#通过sql查询出表里的数据，每次取一行
#
import pymysql
import xlwt

def op_mysql(sql):
    conn = pymysql.connect(
        host='118.24.3.40',
        port=3306,
        user='jxz',
        password='123456',
        db='jxz',
        charset='utf8',
        autocommit=True
    )
    cur = conn.cursor()
    cur.execute(sql)
    result = cur.fetchall()
    cur.close()
    conn.close()
    return result

sql="select * from bai_myuser"
res =op_mysql(sql)

i = 0
r=0
q=0

Table = xlwt.Workbook()
sheet = Table.add_sheet('table')
for readline in res:
    readline=op_mysql(sql)[i]
    for s in readline:
        sheet.write(r, q, s)
        q += 1
    r += 1
    q = 0
    i+=1

Table.save('table.xls')

# 这道题 我唯一没有想到的就是 卡死在那 写入的时候就知道行列用数据 死活不知道怎么写进去  没有想到变量这个问题